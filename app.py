from flask import Flask, request, jsonify
from flask_cors import CORS

from comptask1 import get_lexical_analysis_dist

uid_list = []
username_list = []
password_list = []
app = Flask(__name__)
CORS(app, supports_credentials=True)
app.config['JSON_AS_ASCII'] = False  # 禁止中文转义


@app.route('/')
def hello_world1():
    return 'hello world'


# 编译原理实验1
@app.route('/comptask/1', methods=['POST'])
def comptask1():
    if request.method == 'POST':
        # code = request.form['code']
        # code = str(code)
        data = request.get_json()
        code = data.get("code")
        return jsonify(get_lexical_analysis_dist(code))


if __name__ == '__main__':
    # app.run(debug=True, host='0.0.0.0')  # 传入参数 host：IP地址  debug：是否debug
    app.run(host='0.0.0.0')  # 传入参数 host：IP地址  debug：是否debug
