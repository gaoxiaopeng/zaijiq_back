# 词法分析程序

# 判断是否为定界符
def is_delimiter(code):
    delimiter_list = ['(', ')', '{', '}', ';', ':']
    if code in delimiter_list:
        return True
    return False


# 判断是否为关键字
def is_keyword(code):
    keyword_list = ["int", "main", "double", "void", "if", "else", "switch", "for", "while"]
    if code in keyword_list:
        return True
    return False


# 判断是否为运算符
def is_operator(code):
    operator_list = ['=', '+', '-', '*', '/']
    if code in operator_list:
        return True
    return False


# 判断是否为整数
def is_int(code):
    num_list = ['0', '1', '2', '3', '4', '5', '6', '8', '9']
    for i in code:
        if i not in num_list:
            return False
    return True


# 判断是否为标识符
def is_identifier(code):
    i = code[0]
    if i.isalpha() or i == "_":
        for i in code:
            if not i.isalnum() and i != '_':
                return False
    else:
        return False
    return True


# 切片
def slicing(code):
    word_list = []
    tmp_word = ""
    for i in code:
        if is_operator(i) or is_delimiter(i) or i == ' ':
            if not tmp_word == "":
                word_list.append(tmp_word)
                tmp_word = ""
            if not i == " ":
                word_list.append(i)
        else:
            tmp_word += i
    if not tmp_word == "":
        word_list.append(tmp_word)
    return word_list


# 读取
def lexical_analysis(word_list):
    result_dict = {}
    for i in range(len(word_list)):
        if is_delimiter(word_list[i]):
            result_dict[word_list[i]] = '定界符'
        elif is_operator(word_list[i]):
            result_dict[word_list[i]] = '运算符'
        elif is_keyword(word_list[i]):
            result_dict[word_list[i]] = '关键字'
        elif is_int(word_list[i]):
            result_dict[word_list[i]] = '整型常数'
        elif is_identifier(word_list[i]):
            result_dict[word_list[i]] = '标识符'
    return result_dict


def get_lexical_analysis_dist(code):
    return lexical_analysis(slicing(code))


if __name__ == '__main__':
    input_str = "int main(){int abc=3;}"
    # input_str = "int main(123){int abc=3;}"
    res_dict = lexical_analysis(slicing(input_str))
    # 输出
    print("单词    单词类别")
    for j in res_dict.keys():
        print('{:8}{}'.format(j, res_dict[j]))
